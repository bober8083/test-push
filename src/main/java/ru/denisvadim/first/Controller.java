package ru.denisvadim.first;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller implements Api {

    @Override
    public String ping() {
        return "hello vadim";
    }
}
