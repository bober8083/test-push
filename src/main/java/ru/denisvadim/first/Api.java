package ru.denisvadim.first;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping
public interface Api {
    @GetMapping("/ping-pong")
    String ping();
}
